FROM mysql:5.6

MAINTAINER Cecilia Riveros <criveroscaceres@gmail.com>

COPY 01.-Scripts.sql /docker-entrypoint-initdb.d
